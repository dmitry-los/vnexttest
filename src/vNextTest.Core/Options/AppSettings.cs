﻿namespace vNextTest.Core.Options
{
    public class AppSettings
    {
        #region Log
        public string LogPath { get; set; } 
        #endregion

        #region Database
        public int DbPort { get; set; } 
        public string DbHost { get; set; } 
        public string DbName { get; set; } 
        public string DbUserName { get; set; } 
        public string DbUserPass { get; set; } 
        #endregion

    }
}
