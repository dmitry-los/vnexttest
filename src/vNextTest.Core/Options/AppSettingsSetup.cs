﻿using Microsoft.Framework.OptionsModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vNextTest.Core.Options
{
    public class AppSettingsSetup : ConfigureOptions<AppSettings>
    {
        public AppSettingsSetup()
        : base(ConfigureAppSettings)
        {
        }

        /// <summary>
        /// Set the default options
        /// </summary>
        public static void ConfigureAppSettings(AppSettings options)
        {
            #region Log
            options.LogPath = "D:\\Projects\\vNext";
            #endregion

            #region Database
            options.DbPort = 5432;
            options.DbHost = "localhost";
            options.DbName = "cars";
            options.DbUserName = "postgres";
            options.DbUserPass = "DfGhJc80";
            #endregion
        }
    }
}
