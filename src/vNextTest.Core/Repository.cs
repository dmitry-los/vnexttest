﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using vNextTest.Core.Models;

namespace vNextTest.Core
{
    public class Repository: IRepository
    {
        private static IList<Car> _cars;

        public Repository()
        {
            _cars = _cars ?? new List<Car>();
        }

        public IEnumerable<Car> Get()
        {
            return _cars;
        }

        public Car Get(string number)
        {
            return _cars.FirstOrDefault(c => c.Number == number);
        }

        public bool Add(Car car)
        {
            if (!_cars.Contains(car))
            {
                _cars.Add(car);
                return true;
            }
            return false;
        }

        public bool Delete(string number)
        {
            if (_cars.Any(x => x.Number == number))
            {
                _cars.Remove(_cars.First(c => c.Number == number));
                return true;
            }
            return false;
        }

        public bool Update(Car car)
        {
            if (_cars.Any(c => c.Number == car.Number))
            {
                _cars.Remove(car);
                _cars.Add(car);
                return true;
            }
            return false;
        }
    }
}
