﻿using Microsoft.Framework.OptionsModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vNextTest.Core.Helpers;
using vNextTest.Core.Options;


namespace vNextTest.Core.LogTools
{
    public class Logger : ILogger
    {
        private readonly IOptions<AppSettings> _appSettings;

        public Logger(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings;
        }

        public async Task Log(object obj)
        {
            await FileHelper.WriteAsync(obj, _appSettings.Options.LogPath);
        }
    }
}
