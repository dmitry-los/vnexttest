﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vNextTest.Core.LogTools
{
    interface ILogger
    {
        Task Log(object obj);
    }
}
