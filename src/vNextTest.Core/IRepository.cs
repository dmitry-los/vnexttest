﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vNextTest.Core.Models;

namespace vNextTest.Core
{
    public interface IRepository
    {
        IEnumerable<Car> Get();
        Car Get(string number);
        bool Add(Car car);
        bool Update(Car car);
        bool Delete(string number);
    }
}
