﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vNextTest.Core.Models
{
    public class Car
    {
        public virtual int Id { get; set; }
        public virtual string Brand { get; set; }
        public virtual string Number { get; set; }
        public virtual string Color { get; set; }

        public override string ToString()
        {
            return String.Format("Car Id: {0}, Brand: {1}, Number: {2}, Color: {3}",
                Id, Brand, Number, Color);
        }
    }
}
