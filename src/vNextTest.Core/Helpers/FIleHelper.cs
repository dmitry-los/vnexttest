﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Framework.OptionsModel;
using Newtonsoft.Json;

namespace vNextTest.Core.Helpers
{
    public class FileHelper
    {
        public static async Task WriteAsync(object obj, string filePath)
        {
            var objString = JsonConvert.SerializeObject(obj);
            using (var fileStream = OpenOrCreate(filePath))
            {
                using (var streamWriter = new StreamWriter(fileStream))
                {
                    await streamWriter.WriteAsync(objString);
                }
            }
        }

        private static FileStream OpenOrCreate(string directoryPath)
        {
            var fileName = DateTime.Now.ToString("dd_MM_YY") + ".json";
            var filePath = directoryPath + fileName;

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            if (File.Exists(filePath))
            {
                return File.OpenWrite(filePath);
            }
            return File.Create(filePath);
        }


    }
}
