﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

using vNextTest.Core;
using vNextTest.Core.Models;
using Serilog;

namespace vNextTest.API.Controllers
{
    [Route("api/[controller]")]
    public sealed class CarsController : Controller
    {
        private readonly IRepository _carsRepository;
        
        public CarsController(IRepository repository)
        {
            _carsRepository = repository;
        }

        // GET: api/cars
        [HttpGet]
        public IEnumerable<Car> Get()
        {
            Log.Logger.Debug("GET: api/cars");
            return _carsRepository.Get();
        }

        // GET api/cars/ae1596ax
        [HttpGet("{number}")]
        public Car Get(string number)
        {
            Log.Logger.Debug("GET api/cars/{0}", number);
            return _carsRepository.Get(number);
        }

        // POST api/cars
        [HttpPost]
        public void Post([FromBody]Car car)
        {
            Log.Logger.Debug("POST api/cars; {0}", car.ToString());
            _carsRepository.Add(car);
        }

        // PUT api/cars/
        [HttpPut]
        public void Put([FromBody]Car car)
        {
            Log.Logger.Debug("PUT api/cars; {0}", car.ToString());
            _carsRepository.Update(car);
        }

        // DELETE api/cars/ae1596ax
        [HttpDelete]
        public void Delete(string number)
        {
            Log.Logger.Debug("DELETE api/cars/{0}", number);
            _carsRepository.Delete(number);
        }
    }
}
