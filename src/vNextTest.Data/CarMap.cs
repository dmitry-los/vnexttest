﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using vNextTest.Core.Models;

namespace vNextTest.Data
{
    public class CarMap: ClassMap<Car>
    {
        public CarMap()
        {
            Id(car => car.Id).GeneratedBy.Increment();
            Map(car => car.Brand);
            Map(car => car.Color);
            Map(car => car.Number);
            Table("cars");
        }
    }
}
