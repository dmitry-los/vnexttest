﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.Collections.Generic;

using System;
using System.Linq;
using System.IO;
using vNextTest.Core.Models;
using Microsoft.Framework.OptionsModel;
using Serilog;
using vNextTest.Core;
using vNextTest.Core.Options;

namespace vNextTest.Data
{
    public class CarContext : IRepository
    {
        private readonly ISessionFactory _sessionFactory;
        private readonly IOptions<AppSettings> _appSettings;

        public CarContext(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings;
            _sessionFactory = _sessionFactory ?? CreateSessionFactory();
        }

        public IList<Car> GetAll()
        {
            try
            {
                using (var session = _sessionFactory.OpenSession())
                {
                    using (session.BeginTransaction())
                    {
                        var items = session.CreateCriteria(typeof(Car)).List<Car>();
                        return items;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Warning(ex.Message + ";  " + ex.StackTrace);
                return default(IList<Car>);
            }
        }

        public IEnumerable<Car> Get()
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    var items = session.CreateCriteria(typeof(Car)).List<Car>();
                    return items;
                }
            }
        }

        public Car Get(string number)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    var items = session.CreateCriteria(typeof(Car)).List<Car>();
                    return items.FirstOrDefault(c => c.Number == number);
                }
            }
        }

        public bool Add(Car car)
        {
            return Update(car);
        }

        public bool Update(Car car)
        {
            try
            {
                using (var session = _sessionFactory.OpenSession())
                {
                    using (var trans = session.BeginTransaction())
                    {
                        try
                        {
                            session.SaveOrUpdate(car);
                            trans.Commit();
                            Log.Logger.Debug("Added successfully {0}", car.ToString());
                            return true;
                        }
                        catch (Exception ex)
                        {
                            Log.Logger.Warning(ex.Message + ";  " + ex.StackTrace);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Warning(ex.Message + ";  " + ex.StackTrace);
            }
            return false;
        }

        public bool Delete(string number)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(Get(number));
                        trans.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Log.Logger.Warning(ex.Message + ";  " + ex.StackTrace);
                        return false;
                    }
                }
            }
        }

        private ISessionFactory CreateSessionFactory()
        {
            try
            {
                return Fluently
                    .Configure()
                        .Database(
                            PostgreSQLConfiguration.Standard
                            .ConnectionString(c =>
                                c.Host(_appSettings.Options.DbHost)
                                .Port(_appSettings.Options.DbPort)
                                .Database(_appSettings.Options.DbName)
                                .Username(_appSettings.Options.DbUserName)
                                .Password(_appSettings.Options.DbUserPass)))
                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<CarMap>())
                        .ExposeConfiguration(TreatConfiguration)
                    .BuildSessionFactory();
            }
            catch (Exception ex)
            {
                Log.Logger.Warning(ex.Message + ";  " + ex.StackTrace);
                return default(ISessionFactory);
            }
        }

        private static void TreatConfiguration(Configuration configuration)
        {
            try
            {
                // dump sql file for debug
                Action<string> updateExport = x =>
                {
                    using (var file = new FileStream(@"update.sql", FileMode.Append, FileAccess.Write))
                    using (var sw = new StreamWriter(file))
                    {
                        sw.Write(x);
                        sw.Close();
                    }
                };
                var update = new SchemaUpdate(configuration);
                update.Execute(updateExport, true);
            }
            catch (Exception ex)
            {
                Log.Logger.Warning(ex.Message + ";  " + ex.StackTrace);
            }
        }
    }
}

